package com.backendArabalar.Arabalar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArabalarApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArabalarApplication.class, args);
	}

}
