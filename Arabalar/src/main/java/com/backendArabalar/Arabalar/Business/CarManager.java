package com.backendArabalar.Arabalar.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backendArabalar.Arabalar.DataAccess.ICarDal;
import com.backendArabalar.Arabalar.Entities.cars;


@Service
public class CarManager implements ICarService {

	
	
	private ICarDal iCarDal;

	@Autowired
	public CarManager(ICarDal iCarDal) {
		
		this.iCarDal = iCarDal;
	}

	
	
	
	@Override
	@Transactional
	public List<cars> getAll() {
		return this.iCarDal.getAll();
	}

	@Override
	@Transactional
	public void add(cars car) {
		this.iCarDal.add(car);

	}

	@Override
	@Transactional
	public void delete(cars car) {
		this.iCarDal.delete(car);
	}

	@Override
	@Transactional
	public void update(cars car) {
		this.iCarDal.update(car);

	}




	@Override
	@Transactional
	public cars getById(int id) {
		return this.iCarDal.getById(id);
	}

}
