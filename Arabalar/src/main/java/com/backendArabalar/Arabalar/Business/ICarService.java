package com.backendArabalar.Arabalar.Business;

import java.util.List;

import com.backendArabalar.Arabalar.Entities.cars;


public interface ICarService {
	
	List<cars> getAll();
	void add(cars car);
	void delete(cars car);
	void update(cars car);
	cars getById(int id);


}
