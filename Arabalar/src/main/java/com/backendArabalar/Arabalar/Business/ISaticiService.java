package com.backendArabalar.Arabalar.Business;

import java.util.List;

import com.backendArabalar.Arabalar.Entities.satici;

public interface ISaticiService {

	
	List<satici> getAll();
	void add(satici user);
	void delete(satici user);
	void update(satici user);
	satici getById(int id);
	satici getByPhone(int phone);
}
