package com.backendArabalar.Arabalar.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.backendArabalar.Arabalar.DataAccess.*;

import com.backendArabalar.Arabalar.Entities.satici;

@Service
public class SaticiManager implements ISaticiService {

	
	private ISaticiDal iSaticiDal;
	
	
	
	@Autowired
	public SaticiManager(ISaticiDal iSaticiDal) {
	
		this.iSaticiDal = iSaticiDal;
	}

	@Override
	@Transactional
	public List<satici> getAll() {
		return this.iSaticiDal.getAll();
	}

	@Override
	@Transactional
	public void add(satici user) {
		this.iSaticiDal.add(user);

	}

	@Override
	@Transactional
	public void delete(satici user) {
		this.iSaticiDal.delete(user);

	}

	@Override
	@Transactional
	public void update(satici user) {
	this.iSaticiDal.update(user);
	}

	@Override
	@Transactional
	public satici getById(int conNo) {
		return this.iSaticiDal.getById(conNo);
	}

	public satici getByPhone(int phone) {
		return this.iSaticiDal.getByPhone(phone);
	}
}
