package com.backendArabalar.Arabalar.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;


import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.backendArabalar.Arabalar.Entities.cars;

@Repository
public class HibernateCarDal implements ICarDal {

	//jpa mamager işlemleri 
	private EntityManager entityManager;
	
	@Autowired
	public HibernateCarDal(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<cars> getAll() {
	Session session = entityManager.unwrap(Session.class);
	List<cars> carss=session.createQuery("from cars",cars.class).getResultList();
	return carss;
	}

	@Override
	@Transactional
	public void add(cars car) {
		Session session = entityManager.unwrap(Session.class);
		session.save(car);
	}
	
	@Override
	@Transactional
	public void delete(cars car) {
		Session session = entityManager.unwrap(Session.class);
		cars carDel =session.get(cars.class,car.getId());
		session.delete(carDel);
	}
	
	@Override
	@Transactional
	public void update(cars car) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(car);
	}

	@Override
	@Transactional
	public cars getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		cars car =session.get(cars.class, id);
		return car;
	}

	
}
