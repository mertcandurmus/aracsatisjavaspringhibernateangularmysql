package com.backendArabalar.Arabalar.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.backendArabalar.Arabalar.Entities.satici;



@Repository
public class HibernateSaticiDal implements ISaticiDal {

	private EntityManager entityManager;
	
	
	@Autowired
	public HibernateSaticiDal(EntityManager entityManager) {
		
		this.entityManager = entityManager;
	}

	@Override
	public List<satici> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<satici> saticilar=session.createQuery("from satici",satici.class).getResultList();
		return saticilar;
	}

	@Override
	public void add(satici user) {
		Session session = entityManager.unwrap(Session.class);
		session.save(user);

	}

	@Override
	public void delete(satici user) {
		Session session = entityManager.unwrap(Session.class);
		satici userDel=session.get(satici.class, user.getConNo());
		session.delete(userDel);

	}

	@Override
	public void update(satici user) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(user);
	}

	@Override
	public satici getById(int conNo) {
		Session session = entityManager.unwrap(Session.class);
		satici user=session.get(satici.class, conNo);
		return user;
	}

	@Override
	public satici getByPhone(int phone) {
		Session session = entityManager.unwrap(Session.class);
		satici user=session.get(satici.class, phone);
		return user;
	}
}
