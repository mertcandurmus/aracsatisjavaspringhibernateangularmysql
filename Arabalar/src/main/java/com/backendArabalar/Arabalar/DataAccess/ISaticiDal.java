package com.backendArabalar.Arabalar.DataAccess;

import java.util.List;


import com.backendArabalar.Arabalar.Entities.satici;

public interface ISaticiDal {

	
	List<satici> getAll();
	void add(satici user);
	void delete(satici user);
	void update(satici user);
	satici getById(int id);
	satici getByPhone(int phone);
	
}
