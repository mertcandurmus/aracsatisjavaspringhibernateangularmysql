package com.backendArabalar.Arabalar.Entities;

import javax.persistence.*;

@Entity
@Table(name="cars")
public class cars {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)  //MYSQL=IDENTITY//ORACLE=SEQUENCE
	private int id;
	
	@Column(name="marka")
	private String marka;
	
	@Column(name="model")
	private String model;
	
	@Column(name="yil")
	private String yil;
	
	
	@Column(name="km")
	private String km;
	
	
	@Column(name="fiyat")
	private String fiyat;
	
	
	@Column(name="conno")
	private String conNo;
	
	
	public cars() {
		
	}
	
	
	public cars(int id, String marka, String model, String yil, String km, String fiyat, String conNo) {
		
		this.id = id;
		this.marka = marka;
		this.model = model;
		this.yil = yil;
		this.km = km;
		this.fiyat = fiyat;
		this.conNo = conNo;
	}






	public int getId() {
		return id;
	}






	public void setId(int id) {
		this.id = id;
	}






	public String getMarka() {
		return marka;
	}






	public void setMarka(String marka) {
		this.marka = marka;
	}






	public String getModel() {
		return model;
	}






	public void setModel(String model) {
		this.model = model;
	}






	public String getYil() {
		return yil;
	}






	public void setYil(String yil) {
		this.yil = yil;
	}






	public String getKm() {
		return km;
	}






	public void setKm(String km) {
		this.km = km;
	}






	public String getFiyat() {
		return fiyat;
	}






	public void setFiyat(String fiyat) {
		this.fiyat = fiyat;
	}






	public String getConNo() {
		return conNo;
	}






	public void setConNo(String conNo) {
		this.conNo = conNo;
	}








	
	
	
	
}
