package com.backendArabalar.Arabalar.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="satici")
public class satici {

	@Id
	@Column(name="conNo")
	@GeneratedValue(strategy=GenerationType.IDENTITY)  //MYSQL=IDENTITY//ORACLE=SEQUENCE
	private int conNo;
	
	@Column(name="name")
	private String name;
	
	@Column(name="email")
	private String email;
	
	@Column(name="phone")
	private int phone;
	
	
	public satici() {
		
	}


	public satici(int conNo, String name, String email, int phone) {
		
		this.conNo = conNo;
		this.name = name;
		this.email = email;
		this.phone = phone;
	}


	public int getConNo() {
		return conNo;
	}


	public void setConNo(int conNo) {
		this.conNo = conNo;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public int getPhone() {
		return phone;
	}


	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	
	
	
}
