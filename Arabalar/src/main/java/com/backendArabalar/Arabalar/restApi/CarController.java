package com.backendArabalar.Arabalar.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backendArabalar.Arabalar.Entities.*;

import com.backendArabalar.Arabalar.Business.ICarService;

@RestController
@RequestMapping("/api")

public class CarController {
	
	
	
	private ICarService iCarService;
	@Autowired
	public CarController(ICarService iCarService) {
		
		this.iCarService = iCarService;
	}
	
	
	@GetMapping("/cars")
	public List<cars> get(){
		
		return this.iCarService.getAll();
	}
	
	@PostMapping("/add")
	public void add(@RequestBody cars car) {
		this.iCarService.add(car);
	}
	
	
	@PostMapping("/update")
	public void update(@RequestBody cars car) {
		this.iCarService.update(car);
	}
	
	
	@PostMapping("/delete")
	public void delete(@RequestBody cars car) {
		this.iCarService.delete(car);
	}
	
	@PostMapping("/delete/{id}")
	public void deleteById(@PathVariable int id) {
		cars car = this.iCarService.getById(id);
		this.iCarService.delete(car);
	}
	
	@GetMapping("/cars/{id}")
	public cars getById(@PathVariable int id){
		
		return this.iCarService.getById(id);
	}
	
	
	
	
	

}
