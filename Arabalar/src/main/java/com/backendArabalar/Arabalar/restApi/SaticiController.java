package com.backendArabalar.Arabalar.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.backendArabalar.Arabalar.Business.ISaticiService;
import com.backendArabalar.Arabalar.Entities.*;

@RestController
@RequestMapping("/user")
public class SaticiController {

	
	private ISaticiService iSaticiService;

	
	@Autowired
	public SaticiController(ISaticiService iSaticiService) {
		
		this.iSaticiService = iSaticiService;
	}
	
	@GetMapping("/users")
	public List<satici> get(){
		
		return this.iSaticiService.getAll();
	}
	
	@PostMapping("/add")
	public void add(@RequestBody satici user) {
		this.iSaticiService.add(user);
	}
	
	@PostMapping("/update")
	public void update(@RequestBody satici user) {
		this.iSaticiService.update(user);
	}
	
	@PostMapping("/delete")
	public void delete(@RequestBody satici user) {
		this.iSaticiService.delete(user);
	}
	
	@PostMapping("/delete/{conNo}")
	public void deleteById(@PathVariable int conNo) {
		satici satici1= this.iSaticiService.getById(conNo);
		this.iSaticiService.delete(satici1);
	}
	
	@GetMapping("/users/{conNo}")
	public satici getById(@PathVariable int conNo){
		
		return this.iSaticiService.getById(conNo);
	}
	
	
	@GetMapping("/usersp/{phone}")
	public satici getByPhone(@PathVariable int phone){
		
		return this.iSaticiService.getByPhone(phone);
	}
	
	
	
}
