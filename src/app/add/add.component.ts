import { Component, OnInit } from '@angular/core';
import { Car } from '../models/car';
import { NgForm } from '@angular/forms';
import { CarService } from '../services/car.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [CarService]
})
export class AddComponent implements OnInit {

  constructor(private carService: CarService) { }

  model: Car = new Car();
  showMsg;

  ngOnInit() {
  }

  add1(form: NgForm) {

  this.model.marka = form.value.marka;
  this.model.model = form.value.model;
  this.model.yil = form.value.yil;
  this.model.fiyat = form.value.fiyat;
  this.model.km = form.value.km;
  this.model.conNo = form.value.conNo;
  this.carService.addCars(this.model).subscribe(data => console.log(data), error => console.log(error));
  this.showMsg = true;
}
}
