import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AracComponent } from './arac/arac.component';
import { AddComponent } from './add/add.component';
import { DeleteComponent } from './delete/delete.component';
import { NewUserComponent } from './new-user/new-user.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: 'car', component: AracComponent },
  {path: '', redirectTo: 'car', pathMatch: 'full' },
  {path: 'carCreate', component: AddComponent },
  {path: 'carDelete', component: DeleteComponent },
  {path: 'userCreate', component: NewUserComponent },
  {path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
