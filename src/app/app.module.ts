import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// new imports
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CarService } from './services/car.service';
import { AracComponent } from './arac/arac.component';
import { MenuComponent } from './menu/menu.component';
import { AddComponent } from './add/add.component';
import { DeleteComponent } from './delete/delete.component';
import { CommonModule } from '@angular/common';
import { NewUserComponent } from './new-user/new-user.component';
import { LoginComponent } from './login/login.component';




@NgModule({
  declarations: [
    AppComponent,
    AracComponent,
    MenuComponent,
    AddComponent,
    DeleteComponent,
    NewUserComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule


  ],
  providers: [CarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
