import { Component, OnInit } from '@angular/core';
import { Car } from '../models/car';
import { CarService } from '../services/car.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-arac',
  templateUrl: './arac.component.html',
  styleUrls: ['./arac.component.css'],
  providers: [CarService]
})
export class AracComponent implements OnInit {


  cars: Car[];

  constructor(private carService: CarService) { }

  ngOnInit() {

    this.carService.getCars().subscribe(data => {
      this.cars = data;
    });

  }

}
