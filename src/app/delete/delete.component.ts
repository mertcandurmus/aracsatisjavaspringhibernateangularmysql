import { Component, OnInit } from '@angular/core';
import { CarService } from '../services/car.service';
import { Car } from '../models/car';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {

  constructor(private carService: CarService) { }

  cars: Car[];
  showMsg;

  ngOnInit() {

    this.carService.getCars().subscribe(data => {
      this.cars = data;
    });
  }

  deleteCar(id: number) {
    this.carService.deleteCars(id) .subscribe(
      data => {
        console.log(data);
        this.reloadData();
        this.showMsg = true;
      },
      error => console.log(error));
  }

  reloadData() {
    this.carService.getCars().subscribe(data => {
      this.cars = data;
    });
  }
}
