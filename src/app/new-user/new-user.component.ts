import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { NgForm } from '@angular/forms';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  constructor(private userService: UserService) { }

  model: User = new User();
  showMsg;

  ngOnInit() {
  }


  add1(form: NgForm) {

    this.model.name = form.value.name;
    this.model.phone = form.value.phone;
    this.model.email = form.value.email;
    this.userService.addUser(this.model).subscribe(data => console.log(data), error => console.log(error));
    this.showMsg = true;
    form.reset();
    setTimeout(() => {this.showMsg = false; }, 2000);
  }
}
