import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Car } from '../models/car';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class CarService {

  constructor(private http: HttpClient) { }

path = 'http://localhost:8080/api';



getCars(): Observable<Car[]> {

  return this.http.get<Car[]>(this.path + '/cars').pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );

}

deleteCars(id: number): Observable<Car[]> {

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  return this.http.post<Car[]>(`${this.path}/delete/${id}`, httpOptions).pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );

}


  handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
   errorMessage = 'bir hata oluştu' + err.error.message;
   } else {
     errorMessage = 'sistemsel bir hata';
   }

    return throwError (errorMessage);
  }


addCars(car: Car): Observable<Car> {

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  return this.http.post<Car>(this.path + '/add', car, httpOptions).pipe(
    tap(data => console.log(JSON.stringify(data))),
    catchError(this.handleError)
  );
}

}
