import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }


  path = 'http://localhost:8080/user';


  getUsers(): Observable<User[]> {

    return this.http.get<User[]>(this.path + '/users').pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );

  }


  handleError(err: HttpErrorResponse) {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
   errorMessage = 'bir hata oluştu' + err.error.message;
   } else {
     errorMessage = 'sistemsel bir hata';
   }

    return throwError (errorMessage);
  }




  deleteUser(conNo: number): Observable<User[]> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<User[]>(`${this.path}/delete/${conNo}`, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }


  addUser(user: User): Observable<User> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<User>(this.path + '/add', user, httpOptions).pipe(
      tap(data => console.log(JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

}

